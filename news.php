<?php
/* Template Name: News */

$context = Timber::get_context();
$context['page'] = new TimberPost();
$context['news'] = Timber::get_posts( ['post_type' => 'news_item','posts_per_page' => 20,'orderby' => ['date' => 'DESC'] ] );
Timber::render('news.twig', $context);
?>