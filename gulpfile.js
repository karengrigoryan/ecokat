// dependencies
const
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    image = require('gulp-image'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    uglify = require('gulp-uglify-es').default,
    concat = require('gulp-concat'),
    babel = require('gulp-babel'),
    browserSync = require('browser-sync').create();

// settings
const settings = {
    projectLink: 'ecokat:8888/',
    preferredBrowser: 'firefox'
};

// main paths
const path = {
        assets: './assets',
        scss: './scss',
        img: './assets/img',
        css: './assets/css',
        js: './assets/js'
    },
    scripts = [
        `${path.js}/jquery-3.2.1.min.js`,
        `${path.js}/bootstrap.bundle.min.js`,
        `${path.js}/fontawesome-all.min.js`,
        `${path.js}/lazyload.js`,
        `${path.js}/owl.carousel.min.js`,
        `${path.js}/jquery.swipebox.js`,
        `${path.js}/images.js`,
        `${path.js}/**/*.js`,
        `!${path.js}/app.js`, // exclude main es6 app.js
        `!${path.js}/scripts.min.js`, // exclude minified and concated scripts.min.js
        `${path.js}/app.min.js`
    ];

// optimizing images
gulp.task('images', () => {
    gulp.src(`${path.img}/**/*`)
        .pipe(image())
        .pipe(gulp.dest(`${path.assets}/img/`));
});

// compiling css form scss
gulp.task('scss', () => {
    // error handler
    let onError = function (err) {
        notify.onError({
            title: 'Gulp',
            subtitle: 'Failure!',
            message: 'Error: <%= error.message %>',
            sound: 'Basso'
        })(err);
        this.emit('end');
    };

    return gulp.src(`${path.scss}/styles.scss`)
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass())
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest(path.css));
});

// browserSync settings
gulp.task('browserSync', () => {
    const files = [
        `${path.assets}/**/*`,
        './**/*.twig'
    ];

    browserSync.init(files, {
        notify: false,
        proxy: settings.projectLink,
        open: true,
        browser: settings.preferredBrowser
    });
});

// minifying css
gulp.task('minify-css', ['scss'], () => {
    return gulp.src(`${path.css}/styles.min.css`)
        .pipe(sourcemaps.init())
        .pipe(cleanCSS({compatibility: '*'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.css));
});

// compile es6 to es5
gulp.task('es6', () =>
    gulp.src(`${path.js}/app.js`)
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest(path.js))
);

gulp.task('minify-js', () => {
    return gulp.src(scripts)
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest(path.js));
});

// build/optimize all source files for production
gulp.task('build', ['minify-css', 'minify-js', 'images'], () => {
    console.log('Building files for production');
});

// default gulp task for compiling scss and auto-reloading pages
gulp.task('default', ['browserSync', 'scss', 'es6'], () => {
    gulp.watch(`${path.scss}/**/*.scss`, ['scss']);
    gulp.watch(`${path.js}/app.js`, ['es6']);
});
