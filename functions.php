<?php
// custom jquery
function modify_jquery() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery-3.2.1.min.js', true, '3.2.1');
		wp_enqueue_script('jquery');
	}
}
add_action('init', 'modify_jquery');

// registering menu
function register_menu() {
	register_nav_menu( 'main-menu', __( 'Main Menu' ) );
	register_nav_menu( 'lang-menu', __( 'Languages Menu' ) );
}
add_action( 'init', 'register_menu' );

// calling menu using timber
add_filter( 'timber/context', 'add_to_context' );
function add_to_context( $context ) {
	$context['menu'] = new \Timber\Menu( 'main-menu' );
	$context['langs_menu'] = new \Timber\Menu( 'lang-menu' );
	$context['options'] = get_fields('options');
	$context['homepage'] = new TimberPost(2);
	return $context;
}

// ACF options pages
if ( function_exists( 'acf_add_options_page' ) ) {
	// elements
	acf_add_options_page( array(
			'page_title' => 'Elements'
	) );

	// general information
	acf_add_options_page( array(
			'page_title' => 'General'
	) );
}

// redirecting user to all pages after successful login
function loginRedirect( $redirect_to, $request, $user ){
	if( is_array( $user->roles ) ) {
		return "/wp-admin/edit.php?post_type=page";
	}
}
add_filter("login_redirect", "loginRedirect", 10, 3);

add_action( 'wp_enqueue_scripts', 'custom_contact_script_conditional_loading' );

function custom_contact_script_conditional_loading(){
	//  Edit page IDs here
	if(! is_page(43) )
	{
		wp_dequeue_script('contact-form-7'); // Dequeue JS Script file.
		wp_dequeue_style('contact-form-7');  // Dequeue CSS file.
	}
}