<?php
/* Template Name: Homepage */

$context = Timber::get_context();
$context['posts'] = Timber::get_posts( [ 'post_type' => 'post', 'posts_per_page' => -1, 'orderby' => [ 'date' => 'ASC' ] ] );
$context['news'] = Timber::get_posts( [ 'post_type' => 'news_item', 'posts_per_page' => 2 ] );
Timber::render('index.twig', $context);
?>