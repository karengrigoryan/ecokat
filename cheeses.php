<?php
/* Template Name: Cheeses */

$context = Timber::get_context();
$context['page'] = new TimberPost();
$context['posts'] = Timber::get_posts( [ 'post_type' => 'post', 'posts_per_page' => -1 ] );
$context['categories'] = Timber::get_terms( [ 'orderby' => 'id' ] );
Timber::render('cheeses.twig', $context);
?>