// lazyload
new LazyLoad();

$(function () {
    // main menu nav
    $('[data-toggle-class]').click((e) => {
        const $this = $(e.currentTarget),
            $target = $this.data('target'),
            $class = $this.data('toggle-class');

        // toggle class on target element
        $($target).toggleClass($class);
    });

    // smooth scrolling
    $('.__btn-scroll').click((e) => {
        let $target = $($(e.currentTarget).data('scroll-target')),
            $offset = $(e.currentTarget).data('scroll-offset');

        e.preventDefault();
        $('html, body').animate({
            scrollTop: $target.offset().top - $offset
        }, 1300);
    });

    // owl carousel
    $('#homepage-carousel-cheese').owlCarousel({
        loop: true,
        margin: 30,
        responsiveClass: true,
        nav: true,
        navText: ['<i class="fas fa-2x fa-chevron-left text-primary"></i>', '<i class="fas fa-2x fa-chevron-right text-primary"></i>'],
        dots: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {items: 1},
            768: {items: 2},
            1000: {items: 3},
            1200: {items: 4}
        }
    });
});
